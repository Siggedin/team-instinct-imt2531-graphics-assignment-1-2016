# Grade: C
Like:
  The collision code
Dislike:
  Global variables in the middle of main.cpp
  There is a lot of duplication of code in the movement code.



# Assignment 1

## Group creation deadline 2016/9/30 23:59:59
## Hand in deadline 2016/10/7 23:59:59

In this assignment you will be making Pac Man.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

## Required work
1. Finish the code in ```Level::createWalls()``` and ```WindowHandler::draw(ScreenObject* object)```so that walls are added to the level and can be drawn. The walls do not need to have any fancy textures. Filling an appropriate space with color will do.
2. Create a player object.
    1. Use the ```InputHandler``` to read input from **w, a, s, d** and add movement events to the event queue.
    1. Pop events from the event queue in the update function (_main.cpp_) and use them to move Pac Man around.
    1. Implement collision detection so that Pac Man cannot pass through the walls and can pick up orbs.
    1. Load the provided sprite sheet and animate Pac Man as he moves
5. Create and display objects representing orbs/crates/fruit that can be collected by Pac Man and gives him points. (Sprites/textures optional but it must be visible.) These objects should disapear as Pac Man collects them.
6. Modify the mapfile so that it includes data about where spheres that give points should be placed.
5. Create a text handling class.
    1. Use the text handling class to load the font provided.
    1. Use the font to display a score in the top left corner of the screen as Pac Man picks up orbs.
1. Update this document. (See botom.)

## Restrictions
1. No drawing to screen outside of the ```WindowHandler``` (You are free to modify it as much as you want.)
2. You **must** use the SDL2 library for the graphics.
3. Do not use SDL_TTF for the font handling.

## Suggestions for additional work
1. Add enemies (Colliding with the enemies resets Pac Man to the start position.)
    1. Give the enemies a simple AI.
2. Add functionality that let Pac Man exit on one side of the screen and enter on the other.
3. Add sound.
4. Add more levels and a way to switch between them.
5. Implement proper kerning in the font.
6. Etc

##Group comments
###Who are the members of the group?###
#Asle Vestly Andersen
#Sigbjørn Skar
#Joakim Lebesby

###What did you implement and how did you do it? (Individually)###
Asle:

1. Created the player object which is a ScreenObject stored in the level.

2. Made an enum in ScreenObject to differentiate types of objects.

3. Added movement, synced it by multiplying deltaTime with a set speed and moving pacman by that much.

4. Implemented the animation of pacman, did this by changing the sourceY depending on which direction pacman was moving, and then set sourceX to change by a timer. 

5. Implemented collision by learning from the lazyfoo tutorial and borrowing some of the code.

6. Added orb collision, when colliding the type of the orb is set to empty and will not be drawn.

7. Added fruits that give more points, currently only indicated as a larger orb.

8. Made it so pacman can move from one side of the screen to the other. Very easy change, just check if packman is outside the screen and then move him.

9. After some framework was set up by Joakim I finished the text handling class ScoreCounter. I had a "characters" string store the contents of Arial.bmp, atleast up to 90 characters, up to and including 'z'. Made a for() loop and compared every character to be printed to "characters", if equal it draws it to the renderer by calculating where on the image it is stored by using its position in the "characters" string 

Sigbjørn:

I started working with code for walls and orbs, when Asle were finishing the codes for fruits we eventually merged all the inits functions and made and initObjects function. I did work on some of the code for scorecounter and just general helping out elsewhere in the code.

Asle and Sigbjørn:
We collaborated on implementing the orbs and drawing them.

We all collaborated and contributed to the text handling class.

###What parts if any of the base code did you change and why?###
We mostly just added stuff, but we did repurpose the walls object vector to simply store all objects and renamed it to "objects", getObjects(), etc.
###What was the hardest part of this assignment? (Individually)###
Asle: Definitely the text handling class, had to work through multiple sticky bugs to get it working and try a couple of different approaches.

Sigbjørn: Me and Joakim personally spent some time at the beginning trying to learn the code and the basics of sdl. The text handling class was probably what we spent the most time solving. (different bugs etc). 
###Did you feel like the assignment was an appropriate amount of work? (Individually)###
Asle: Not sure, maybe if the workload was divided better. We had some problems distributing work and the teamwork wasn't that great in the beginning, but it got a lot better towards the end. Everyone worked on this a fair amount, but the others didn't have the same amount of programming experience as me and time went into understanding the code and learning SDL.

Sigbjørn: The same things Asle is pointing out. Probably have a more structured working plan or something aswell. 

Joakim:  Probably too hard for those not fluent in coding.
###Other comments###