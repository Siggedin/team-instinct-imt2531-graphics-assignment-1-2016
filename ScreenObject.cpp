#include "ScreenObject.h"

glm::vec2 ScreenObject::getPosition() {
	return position;
}

SDL_Rect ScreenObject::getSourceRect()
{
	SDL_Rect rect;

	rect.x = static_cast<int>(scPosition.x);
	rect.y = static_cast<int>(scPosition.y);
	rect.w = static_cast<int>(scSize.x);
	rect.h = static_cast<int>(scSize.y);

	return rect;
}

SDL_Rect ScreenObject::getDestRect()
{
	SDL_Rect rect;

	rect.x = static_cast<int>(position.x);
	rect.y = static_cast<int>(position.y);
	rect.w = static_cast<int>(size.x);
	rect.h = static_cast<int>(size.y);

	return rect;
}

objectType ScreenObject::getType(){
	return type;
}
void ScreenObject::setType(objectType tp){
	type = tp;
}

void ScreenObject::setDestRect(glm::vec2 pos, glm::vec2 sz)
{
	position = pos;
	size = sz;
}
void ScreenObject::setSourceRect(glm::vec2 pos, glm::vec2 sz)
{
	scPosition = pos;
	scSize = sz;
}
void ScreenObject::setSourcePos(glm::vec2 pos){
	scPosition = pos;
}
void ScreenObject::modDestX(float x){
	position.x += x;
}
void ScreenObject::modDestY(float y){
	position.y += y;
}