#pragma once

#include <SDL2/SDL.h>
#include <vector>
#include <glm/glm.hpp>
#include <math.h>

#include "ScoreCounter.h"
#include "ScreenObject.h"

class WindowHandler
{
public:
	//This ensures that there can only be one WindowHandler object at any given time.
	static WindowHandler& getInstance()
	{
		static WindowHandler instance;
		return instance;
	}

	bool init();
	void clear();//Clears the back buffer, making it ready for a new frame.
	//Loops through a list of ScreenObjects and calls the draw function on them.
	//This is used to prevent exessive calling of getInstance()
	void drawList(std::vector<ScreenObject>* objects);
	void drawBackground();
	void drawScore();
	void draw(ScreenObject* object); //This should draw a ScreenObject object or a child class there of.
	void initTexture(ScreenObject* object);
	SDL_Texture* convertSurface(SDL_Surface* surface);
	void update(); //Switches the front and back buffer
	void close();

	SDL_Surface* getSurface();
	glm::vec2 getScreenSize();

   // Mix_Music* music = null;

	//This ensures that there can only be one WindowHandler object at any given time.
	WindowHandler(WindowHandler const& copy) = delete;
	void operator=(WindowHandler const& copy) = delete;
private:
	SDL_Rect background;
	SDL_Window* window = nullptr;
	SDL_Surface* screenSurface = nullptr;
	SDL_Renderer* renderer = nullptr;
	char windowName[100];
	int windowXSize;
	int windowYSize;

	WindowHandler() {};
	bool initSDL();
	void loadConfig();
};
