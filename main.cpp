#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "globals.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Level.h"

//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels()
{
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++)
	{
		char tempCString[51];
		fscanf(file, "%50s", &tempCString);
		tempString = tempCString;
		Level level(tempString);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;

	return &gLevels.front();
}
/*
void loadMusic()
{



}
*/

//Initializes the InputHandler and WindowHandler
//The WindowHandler intitilizes SDL
//Loads levels and returns a pointer to the first Level by calling loadLevels()
Level* init()
{
	InputHandler::getInstance().init();
	if(!WindowHandler::getInstance().init()) {
		gRunning = false;
	}
	ScoreCounter::getInstance().init();

	Level* loadedLevels = loadLevels();
	loadedLevels->initPlayer();
	loadedLevels->initOrbs();

	return loadedLevels;
}

float timer = 0.0f;	//Variable used to time the animations
int animationState = 0;	//Variable used to track where in the animation sequence we are
int sourceX = 0;
int sourceY = 0;
int speed = 40;
//While there are events in the eventQueue. Process those events.
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Level* currentLevel)
{
	GameEvent nextEvent;
	float screenWidth = currentLevel->getDimensions().x * currentLevel->getSquareSize().x;
	std::vector<ScreenObject>* objectPointer = currentLevel->getObjects();
	std::vector<ScreenObject> object = *objectPointer;
	ScoreCounter::getInstance().setOutput("Score: " + std::to_string(ScoreCounter::getInstance().getScore()));

	while(!eventQueue.empty())
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();

		timer += deltaTime;
		if(timer > 0.10)	//Sync this if we have time
		{
			timer = 0;
			//Do this IF the player is moving
			if(nextEvent.action == ActionEnum::PLAYER_MOVE_UP || nextEvent.action == ActionEnum::PLAYER_MOVE_RIGHT ||
			nextEvent.action == ActionEnum::PLAYER_MOVE_DOWN || nextEvent.action == ActionEnum::PLAYER_MOVE_LEFT ||
			animationState != 1) // returns to default position after the player stops moving
			{
				animationState++;
				if(animationState == 1){ sourceX = 218; }
				if(animationState == 2){ sourceX = 144; }
				if(animationState == 3){ sourceX = 72; }
				if(animationState == 4){ sourceX = 0; }
				if(animationState == 5){ sourceX = 0; }
				if(animationState == 6){ sourceX = 72; }
				if(animationState == 7){ sourceX = 144; }
				if(animationState == 8){ animationState = 0; }
			}
		}


		if(nextEvent.action == ActionEnum::PLAYER_MOVE_UP)
		{

			currentLevel->getPlayer()->modDestY(-deltaTime * speed);
			for(unsigned int i = 0; i < currentLevel->getObjects()->size(); i++)
			{
				if(checkCollision(currentLevel->getPlayer()->getDestRect(), object[i].getDestRect()))
				{
					if(object[i].getType() == Wall){
						currentLevel->getPlayer()->modDestY(deltaTime * speed);
					}
					if(object[i].getType() == Orb){
						objectPointer->at(i).setType(Empty);
						ScoreCounter::getInstance().modScore(10);
					}
					if(object[i].getType() == Fruit){
						objectPointer->at(i).setType(Empty);
						ScoreCounter::getInstance().modScore(100);
					}

				}
			}
			sourceY = 72;
		}
		if(nextEvent.action == ActionEnum::PLAYER_MOVE_RIGHT)
		{
			currentLevel->getPlayer()->modDestX(deltaTime * speed);
			for(unsigned int i = 0; i < currentLevel->getObjects()->size(); i++)
			{
				if(checkCollision(currentLevel->getPlayer()->getDestRect(), object[i].getDestRect()))
				{
					if(object[i].getType() == Wall)
						currentLevel->getPlayer()->modDestX(-deltaTime * speed);
					if(object[i].getType() == Orb){
						objectPointer->at(i).setType(Empty);
						ScoreCounter::getInstance().modScore(10);
					}
					if(object[i].getType() == Fruit){
						objectPointer->at(i).setType(Empty);
						ScoreCounter::getInstance().modScore(100);
					}
				}
			}
			sourceY = 218;
			if(currentLevel->getPlayer()->getPosition().x + currentLevel->getPlayer()->getDestRect().w > screenWidth)
				currentLevel->getPlayer()->modDestX(-screenWidth);	//If the player leaves the screen, pop up at the other side
		}
		if(nextEvent.action == ActionEnum::PLAYER_MOVE_DOWN)
		{
			currentLevel->getPlayer()->modDestY(deltaTime * speed);
			for(unsigned int i = 0; i < currentLevel->getObjects()->size(); i++)
			{
				if(checkCollision(currentLevel->getPlayer()->getDestRect(), object[i].getDestRect()))
				{
					if(object[i].getType() == Wall)
						currentLevel->getPlayer()->modDestY(-deltaTime * speed);
					if(object[i].getType() == Orb){
						objectPointer->at(i).setType(Empty);
						ScoreCounter::getInstance().modScore(10);
					}
					if(object[i].getType() == Fruit){
						objectPointer->at(i).setType(Empty);
						ScoreCounter::getInstance().modScore(100);
					}
				}
			}
			sourceY = 0;
		}
		if(nextEvent.action == ActionEnum::PLAYER_MOVE_LEFT)
		{
			currentLevel->getPlayer()->modDestX(-deltaTime * speed);
			for(unsigned int i = 0; i < currentLevel->getObjects()->size(); i++)
			{
				if(checkCollision(currentLevel->getPlayer()->getDestRect(), object[i].getDestRect()))
				{
					if(object[i].getType() == Wall)
						currentLevel->getPlayer()->modDestX(deltaTime * speed);
					if(object[i].getType() == Orb){
						objectPointer->at(i).setType(Empty);
						ScoreCounter::getInstance().modScore(10);
					}
					if(object[i].getType() == Fruit){
						objectPointer->at(i).setType(Empty);
						ScoreCounter::getInstance().modScore(100);
					}
				}
			}
			sourceY = 144;
			if(currentLevel->getPlayer()->getPosition().x < -currentLevel->getPlayer()->getDestRect().w)
				currentLevel->getPlayer()->modDestX(screenWidth);
		}
	}
	currentLevel->getPlayer()->setSourceRect(glm::vec2(sourceX, sourceY), glm::vec2(72, 72));
}

//This is the main draw function of the program. All calls to the WindowHandler for drawing purposes should originate here.
void draw(Level* currentLevel)
{
	WindowHandler::getInstance().clear();
	WindowHandler::getInstance().drawBackground();
	WindowHandler::getInstance().drawList(currentLevel->getObjects());
	WindowHandler::getInstance().draw(currentLevel->getPlayer());
	WindowHandler::getInstance().drawScore();
	WindowHandler::getInstance().update();
}

//Calls cleanup code on program exit.
void close() {
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[])
{
	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	Level* currentLevel = nullptr;

	float nextFrame = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime = 0.0f; //Time since last pass through of the game loop.
	auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop = clockStart;

	currentLevel = init();

	while(gRunning)
	{
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue, currentLevel);

		if(nextFrameTimer >= nextFrame)
		{
			draw(currentLevel);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}
