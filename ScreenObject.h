#pragma	once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <glm/glm.hpp>

enum objectType{ Wall, Player, Enemy, Fruit, Orb, ScoreCounter, Empty, Ghost };

class ScreenObject 
{
public:
	ScreenObject() {};
	//ScreenObject(glm::vec2 pos, glm::vec2 sz) : position(pos), size(sz){};
	ScreenObject(glm::vec2 pos, glm::vec2 sz, objectType tp) : type(tp), position(pos), size(sz){};
	~ScreenObject() {};

	static ScreenObject& getInstance() 
	{
		static ScreenObject instance;
		return instance;
	}
	
	glm::vec2 getPosition();
	SDL_Rect getSourceRect();
	SDL_Rect getDestRect();
	objectType getType();

	void setSourceRect(glm::vec2 pos, glm::vec2 sz);
	void setSourcePos(glm::vec2 pos);
	void setDestRect(glm::vec2 pos, glm::vec2 sz);
	void setType(objectType tp);

	void modDestX(float x);
	void modDestY(float y);

	SDL_Surface* spriteSheet;
	SDL_Texture* texture;
	static SDL_Texture* orbTexture;
protected:
	objectType type;
	glm::vec2 position;
	glm::vec2 size;
	glm::vec2 scPosition;
	glm::vec2 scSize;
};
