#include "ScoreCounter.h"

void ScoreCounter::init()
{	//Initialize Score counter, mirroring pacman image load
	setSourceRect(glm::vec2(0, 0), glm::vec2(21, 25));
	setDestRect(glm::vec2(5, 5), glm::vec2(20, 20));
	spriteSheet = IMG_Load("Arial.bmp");
	texture = WindowHandler::getInstance().convertSurface(&*spriteSheet);
	characters = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz"; //"'
}

SDL_Rect ScoreCounter::getSourceRect()
{
	SDL_Rect rect;
	rect.x = static_cast<int>(scPosition.x);
	rect.y = static_cast<int>(scPosition.y);
	rect.w = static_cast<int>(scSize.x);
	rect.h = static_cast<int>(scSize.y);
	return rect;
}
SDL_Rect ScoreCounter::getDestRect()
{
	SDL_Rect rect;
	rect.x = static_cast<int>(position.x);
	rect.y = static_cast<int>(position.y);
	rect.w = static_cast<int>(size.x);
	rect.h = static_cast<int>(size.y);
	return rect;
}
void ScoreCounter::setSourceRect(glm::vec2 pos, glm::vec2 sz)
{
	scPosition = pos;
	scSize = sz;
}
void ScoreCounter::setDestRect(glm::vec2 pos, glm::vec2 sz)
{
	position = pos;
	size = sz;
}
void ScoreCounter::setSourcePos(glm::vec2 pos){
	scPosition = pos;
}
void ScoreCounter::setDestPos(glm::vec2 pos){
	position = pos;
}

void ScoreCounter::modSourceX(float x){
	scPosition.x += x;
}
void ScoreCounter::modSourceY(float y){
	scPosition.y += y;
}
void ScoreCounter::modDestX(float x){
	position.x += x;
}
void ScoreCounter::modDestY(float y){
	position.y += y;
}
void ScoreCounter::modScore(int s){
	score += s;
}

int ScoreCounter::getScore(){
	return score;
}
int ScoreCounter::getOutputSize(){
	return output.length();
}
char ScoreCounter::getChar(int i){
	return characters[i];
}
std::string ScoreCounter::getOutput(){
	return output;
}

void ScoreCounter::setOutput(std::string input){
	output = input;
}