#pragma	once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <glm/glm.hpp>
#include <string>
#include "WindowHandler.h"
#include "ScreenObject.h"

class ScoreCounter : public ScreenObject
{
public:
	static ScoreCounter& getInstance()
	{
		static ScoreCounter instance;
		return instance;
	}

	ScoreCounter() {};
	ScoreCounter(glm::vec2 pos, glm::vec2 sz) : position(pos), size(sz){};
	~ScoreCounter() {};

	SDL_Rect getSourceRect();
	SDL_Rect getDestRect();
	void setSourceRect(glm::vec2 pos, glm::vec2 sz);
	void setDestRect(glm::vec2 pos, glm::vec2 sz);
	void setSourcePos(glm::vec2 pos);
	void setDestPos(glm::vec2 pos);

	void setOutput(std::string input);

	void modSourceX(float x);
	void modSourceY(float y);
	void modDestX(float x);
	void modDestY(float y);
	void modScore(int s);

	int getScore();
	int getOutputSize();
	char getChar(int i);
	std::string getOutput();

	void init();

	SDL_Surface* spriteSheet;
	SDL_Texture* texture;
protected:
	glm::vec2 position;
	glm::vec2 size;
	glm::vec2 scPosition;
	glm::vec2 scSize;
	int score;
	std::string characters;
	std::string output;
	int outputSize;
};
