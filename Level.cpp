#include "Level.h"
#include "WindowHandler.h"

Level::Level(std::string filepath)
{
	loadMap(filepath);
	createObjects();
}

/*
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space
2 represents Pac Mans start position
*/
void Level::loadMap(std::string filepath)
{
	int x, y, temp;
	FILE* file = fopen(filepath.c_str(), "r");

	fscanf(file, "%dx%d", &x, &y);

	for(int i = 0; i<y; i++)
	{
		std::vector<int> row;
		for(int j = 0; j<x; j++)
		{
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}
	dimensions = glm::vec2(x, y);
	glm::vec2 screenSize = WindowHandler::getInstance().getScreenSize();
	//Have the size of a square adapt to the size of the screen
	squareSize = glm::vec2(screenSize.x / dimensions.x, screenSize.y / dimensions.y);

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects
void Level::createObjects()
{
	SDL_Surface* orbImg = IMG_Load("Orb.png");

	int rows = map.size();
	int cells = 0;

	for(int i = 0; i<rows; i++)
	{
		cells = map[i].size();

		for(int j = 0; j<cells; j++)
		{
			if(map[i][j] == 1)
			{
				//Creates a wall at its position, which depends on the size of the square
				ScreenObject wall(glm::vec2(j * squareSize.x, i * squareSize.y), squareSize, objectType::Wall);                                                                                                                                                                        // wall.positionUpdate(map[i][j])
				objects.push_back(wall);
			}
			if(map[i][j] == 0)
			{
				ScreenObject orb(glm::vec2(j * squareSize.x + squareSize.x / 4, i * squareSize.y + squareSize.y / 4), glm::vec2(squareSize.x / 2, squareSize.y / 2), objectType::Orb);
				orb.setSourceRect(glm::vec2(0, 0), glm::vec2(100, 100));
				orb.spriteSheet = orbImg;
				WindowHandler::getInstance().initTexture(&orb);	//Make global texture?
				objects.push_back(orb);
			}
			if(map[i][j] == 2) //Fruits
			{
				ScreenObject fruit(glm::vec2(j * squareSize.x, i * squareSize.y), glm::vec2(squareSize.x, squareSize.y), objectType::Fruit);
				fruit.setSourceRect(glm::vec2(0, 0), glm::vec2(100, 100));
				fruit.spriteSheet = orbImg;
				WindowHandler::getInstance().initTexture(&fruit);
				objects.push_back(fruit);
			}
		}
	}
}

void Level::initPlayer()
{	//Initialize player values, there is probably a better way of doing this
	player.setSourceRect(glm::vec2(0, 0), glm::vec2(72, 72));
	player.setDestRect(glm::vec2(280, 400), glm::vec2(20, 20));
	player.setType(objectType::Player);
	player.spriteSheet = IMG_Load("pacman.png");
	WindowHandler::getInstance().initTexture(&player);
}

void Level :: initOrbs()
 {

    SDL_Surface* orbpic= IMG_Load("Basketball.png");
    int sq_size = 10;
    glm::vec2 size = glm::vec2(sq_size, sq_size);
    int rows = map.size();
 	int cells = 0;                                                                                                                                                                     //	glm::vec2 size = glm::vec2(squareSize, squareSize);


 	for(int i = 0; i<rows; i++)
 	{
 		cells = map[i].size();
 		for (int j = 0; j<cells; j++)
 		{
 			if(map[i][j] == 0)
 			{
           ScreenObject orb(glm::vec2(j * squareSize.x+5, i * squareSize.y+5), size , objectType::Orb);
           orb.spriteSheet = orbpic;
           orb.setSourceRect(glm::vec2(0,0), glm::vec2(340,340));

            	//orbs.push_back(orb);
 			}
 		}
 	}

 }


std::vector<ScreenObject>* Level::getObjects(){
	return &objects;
}
ScreenObject* Level::getPlayer(){
	return &player;
}

ScreenObject* Level::getScore(){
	return &scoreCounter;
}

glm::vec2 Level::getDimensions(){
	return dimensions;
}
std::vector<std::vector<int> > Level::getMap(){
	return map;
}

glm::vec2 Level::getSquareSize(){
	return squareSize;
}

