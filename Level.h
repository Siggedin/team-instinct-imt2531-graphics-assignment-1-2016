#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>

#include "ScreenObject.h"

class Level
{
public:
	Level(std::string filepath);

	std::vector<ScreenObject>* getObjects();
	std::vector<std::vector<int> > getMap();
	std::vector<ScreenObject>* getOrbs();
	ScreenObject* getPlayer();
	ScreenObject* getScore();
	glm::vec2 getDimensions();
	glm::vec2 getSquareSize();

	void initPlayer();
	void initOrbs();

private:
	std::vector<std::vector<int> > map;
	std::vector<ScreenObject> objects;
	glm::vec2 dimensions;
	glm::vec2 squareSize;
	ScreenObject player;
	ScreenObject scoreCounter;
	ScreenObject orb;

	void loadMap(std::string filepath); //Loads map data from file
	void createObjects();	//This should create a ScreenObject for each wall segment.
};
